=======================================
README file for pyfred server & clients
=======================================

.. toctree::
   :numbered:
   :caption: Table of Contents
   
   Introduction
   Servers' and clients' description
     Filemanager
     Genzone
       Configuration of genzone client
    Repository overview
    Configuration
    Dependencies
    Testing


Introduction
============

Pyfred is part of FRED system (system for management of internet domains)
and includes various servers and clients both written in python.
In fact there is just one general server which encapsulates specialized
servers, which are often referred to as modules. It is a plugin architecture.
The underlying communication infrastructure is CORBA. Pyfred clients
communicate with servers by RMI paradigm, the interface is specified in
IDL files, which are not part of pyfred's repository, but are centrally
managed elsewhere.


Servers' and clients' description
=================================

There are following servers each of which has at least one client:

 +--------------------+-----------------------------------------+
 |        SERVER      |                     CLIENTS             |
 +====================+=========================================+
 |  filemanager       |          client and admin               |
 +--------------------+-----------------------------------------+
 |  genzone           |          client and test                |
 +--------------------+-----------------------------------------+

Note: Content of clients column needs some explanation. Almost every interface
      defines two kinds of functions: custom and administrative. Custom
      functions fulfil the purpose of server's existence, it means that
      without these functions there is no reason for server to exist.
      Adminstrative functions constitute completion to normal operation of
      server. They implement more advanced functionality of server. These
      two groups of functions are separated in different files (in different
      clients). Genzone has test client, which is a script intended to be
      used by nagios monitoring software.


Filemanager
-----------

FileManager daemon is capable of storing and loading files. FileManager
stores files on filesystem under directory which is given in configuration.
FileManager does actually more than just storing or loading file. It keeps
some other information about managed files in database. Currently it is:

* Numeric id of file
* Date of creation of record (file's upload date)
* Human readable and friendly name of file
* MIME type of file
* Size of file in bytes
* Path to file on filesystem in repository
* Type of file (one of: invoice pdf, invoice xml, accounting xml,
  banking statement)

This set of information can be retrieved by a corba method call and by
another one can be downloaded the file from server. There is also a function
for upload of a file to server. The files are downloaded and uploaded
in client selected chunks in order not to exceed the data size limit
of one corba call.

It is possible to search files based on various criteria, but true
administration interface capable of deletion and manipulation with files
is not yet in place and is not planned for the near future.

Information about files is kept in database compared to files' content, which
is kept on filesystem. The files on filesystem are not stored in flat manner,
but are distributed in subdirectories in order not to overfill one directory.
The relative path is created as:

    {year}/{month}/{day}/{id}

where year, month and day are deduced from date when the file was uploaded
to server. The name of the file on filesystem, here denoted as id, is
primary key of file's record in database and should not be confused with
alias (human-friendly name of file) which is stored as attribute of file
in database and may not be unique.

There are two clients available for FileManager server. One is capable of
download/upload and the second (administrative) of searching in database
of files.


Genzone
-------

Goal of the zone generator server is to send all data needed for zone
file creation to a client. It is a task of client to format data in a way,
the DNS server understands to. The tasks of server are:

* Generate data which should be in a zone based on status flag
  'outzone'.
* Send resulting data through CORBA interface to client in several
  chunks by how big they are.

Zone generator utilizes results of another process, which sets status' for
objects in database. The zone generator simply picks domains, which don't have
status set to 'outzone'.

These constrains must be met in order for domain to be placed in zone. The list
of conditions here is not complete, since the zonegenerator is not responsible
for checking of these conditions.

* Domain must have associated nsset.
* Domain must not be expired and additionaly if it is an enum domain,
  validation must be still valid. By 'expired' we mean that
  domain is out of safe-period (which is about one month after the
  real expiration date).
* The domain must not have set status 'outZoneManual', it means
  the domain was not manually outaged from zone.

A nameserver which requires GLUE by DNS standard is considered valid
only if it has one. On the other side, if nameserver has a GLUE and the GLUE
is not required, the GLUE is not passed to client but the nameserver is.
Neither of mentioned inconsistencies influences placement or displacement
of a domain in a zone. Only warning messages are logged to syslog.

The client is responsible for formatting of received data in manner which is
acceptable by DNS server.  The only currently availabel output format is a
format of BIND DNS server. Second client script is dedicated solely to testing
of genzone server. Zone generation is so important service that there is a
special need to test, that server is up and running. Output of test script is
compatible with expectations of Nagios monitoring software.  Genzone client
handles also backups of old zone files before they are overwritten by newly
generated zone files. It is possible to configure post-hook for each zone (or
in DEFAULT section for all zones).  Hook is a command which is executed after
the zone is generated.  Genzone performs substitution for following tokens in
hook command line:

$file     absolute path of new generated zone file
$zone     name of zone which is generated
$backup   absolute path of previous (backed up) zone file

The hook is trigered only if the zone file is successfully generated. By this
hook you can conveniently restart a bind server for example.

Zone generation support generation of DS records from server keyset objects
attached to domains.

Configuration of genzone client
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Genzone client is configurable by configuration file. The default location
is /etc/fred/genzone.conf, but you may override this default from command
line. The configuration file may contain individual section for each zone.
Configuration parameters, which are not specific to a zone, are in section
called 'general'. The command line parameters are preferred to values
from configuration file, but beware that options on command line override
configuration of all generated zones. The description of configuration
directives follow.

General configuration directives:

* nameservice [localhost] - Host where is running corba nameservice.

* context [fred] - Corba nameservice context (directory)

* verbose [Off] - If the client should be verbose when generating zones.

* zones [] - Space separated list of zone names to be generated. This
  list is only used, if no zones were specified on command line. If no
  zones are specified at all, list of zones is downloaded from server

Zone specific configuration directives:

* zonedir [./] - Directory where new zone files should be generated.
  Default is current directory.

* backupdir [] - Directory where current zone file from zonedir should
  be backuped before being overwritten by new zone file. Default is
  to use the same directory as zonedir.

* nobackups [False] - It set to True, no backups are created at all
  (backupdir directive is in that case useless).

* chunk [100] - Relates to mechanism of how the records are transfered
  over corba. It means how many domains are transferred in one CORBA
  call.

* header [] - If defined, the content of header file will be prepended
  to zone file.

* footer [] - If defined, the content of footer file will be appended
  to zone file.

* maxchanges [-1] - The maximal number of changed records between the
  new zone file and previous zone file. If The number is negative, it
  means that the is no such a limit. If you prepend '%' character to
  the number, it means procentual change.


Repository overview
===================

Here we describe what is in which directory and where it is installed.

pyfred directory - is a python package which contains:

* 'zone' module used by genzone client for communication with CORBA server.
* 'utils' module which gathers various functions shared by corba servers.
* 'modules' package which contains modules implementing the CORBA servers.

scripts directory - contains python scripts (pyfred server and all clients)
which are installed in $PATH. There is also start/stop/status pyfredctl
script.


Configuration
=============

The pyfred server looks for its configuration file (pyfred.conf) in current
directory, /etc/fred or /usr/local/etc/fred in this order. When starting it
prints, which config file it is using. You can explicitly set configuration
file on command line.

Configuration file is organized in sections. One called General which is not
specific to any CORBA server and influences general behaviour shared by
all CORBA servers, the other sections are dedicated to individual CORBA
servers.

Before we will look at configuration directives in depth, we will explain
meaning of 2 directives which are common for all CORBA servers (it means
they can be set in all sections except section General). It is 'idletreshold'
and 'checkperiod' used to control lifetime of dynamically created CORBA
objects, both accept number of seconds. Every CORBA server has by coincidence
need to spawn new CORBA objects, which are dedicated to one simple task
(i.e. to transfer bytes of file or to transfer a zone records).

* idletreshold [3600] - says how many seconds the object must not be
  accessed by any CORBA method in order to be taken as "death" and will be
  automically deleted.

* checkperiod [60] - is interval in which are checked objects for being idle.

The following rule should hold: checkperiod <= idletreshold.

The sections will be examined in following order:

1. General
2. Genzone
3. FileManager

The default values for directive is enclosed in square brackets. If there is
no default value, the square brackets are missing.

General
-------

* modules - list of modules (CORBA servers) which should be loaded and started.

* dbhost - Database host (empty value means: use unix socket).

* dbport [5432] - Database port (5432 is standard postgresql's port).

* dbuser [fred] - Database username.

* dbpassword - Database password.

* dbname [fred] - Database name to use.

* nshost [localhost] - Host where CORBA nameservice runs.

* nsport [2809] - Port on which CORBA nameservice listens (2809 is standard).

* context [fred] - Corba nameservice context to use for binding objects.
  For example if we select 'fred' context then the object genzone will
  be known by nameservice as: 'fred.context/genzone.object'.

* loglevel [LOG_INFO] - Syslog level used for logging (LOG_DEBUG,
  LOG_NOTICE, LOG_INFO, LOG_WARNING, LOG_ERR, LOG_EMERG, LOG_CRIT).

* logfacility [LOG_LOCAL1] - Syslog facility used for logging (see manual
  page of syslog for possible facilities).

* piddir [/var/run] - Directory where pidfile pyfred.pid is created.

* host [] - IP address or hostname where pyfred listens. If empty then
  pyfred binds to all available IP addresses.

* port [2225] - Port where pyfred server listens.

Genzone
-------

* idletreshold and checkperiod influence lifetime of CORBA objects dedicated
  for zone transfers.

Dependencies
============

pyfred servers and clients assume that certain packages are installed on your
system. If you want to test your system for presence of dependecies (which is
strongly recommended to do anyway), type:

$ python setup.py config

The output of this command reminds strongly output of a configure script
generated by autoconf.

Here are explicitly listed the dependencies:

* Linux operating system
* Python version 2.5 or higher
* python bindings for omniORB library
* pygresql package
* python bindings for clearsilver library


Testing
=======

There are unittests in directory "unittests". Currently there is only test
for genzone component. For more information about unittest see comments in
file.

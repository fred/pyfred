#!/usr/bin/python2
#
# Copyright (C) 2007-2023  CZ.NIC, z. s. p. o.
#
# This file is part of FRED.
#
# FRED is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# FRED is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with FRED.  If not, see <https://www.gnu.org/licenses/>.

from setuptools import setup

ENTRY_POINTS = {'console_scripts': [
    'check_pyfred_genzone = pyfred.commands.check_pyfred_genzone:run_check_pyfred_genzone',
    'filemanager_admin_client = pyfred.commands.filemanager_admin_client:run_filemanager_admin_client',
    'filemanager_client = pyfred.commands.filemanager_client:run_filemanager_client',
    'fred-pyfred = pyfred.commands.fred_pyfred:run_fred_pyfred',
    'genzone_client = pyfred.commands.genzone_client:run_genzone_client',
    'pyfredctl = pyfred.commands.pyfredctl:run_pyfredctl']}


def main():
    setup(name="fred-pyfred",
          version="2.16.0",
          description="Component of FRED (Fast Registry for Enum and Domains)",
          author="Jan Kryl",
          author_email="jan.kryl@nic.cz",
          url="http://fred.nic.cz/",
          license="GPLv3+",
          platforms=['posix'],
          packages=("pyfred", "pyfred.commands", "pyfred.unittests", "pyfred.modules"),
          include_package_data=True,
          python_requires='~=2.7',
          install_requires=['PyGreSQL>=5'],
          entry_points=ENTRY_POINTS)


if __name__ == '__main__':
    main()
